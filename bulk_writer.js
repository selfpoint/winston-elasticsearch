/* eslint no-underscore-dangle: ['error', { 'allow': ['_index', '_type'] }] */

const fs = require('fs');
const path = require('path');
const Promise = require('promise');
const debug = require('debug')('winston:elasticsearch');
const retry = require('retry');
const CHECK_CONNECTION_INTERVAL = 15 * 60 * 1000; // 15 min
const EMIT_BULK_LENGTH_INTERVAL = 10 * 60 * 1000; // 15 min

let _lastLog;

class BulkWriter {
  constructor(transport, client, options) {
    this.transport = transport;
    this.client = client;
    this.options = options;
    this.interval = options.interval || 5000;
    this.healthCheckTimeout = options.healthCheckTimeout || '30s';
    this.healthCheckWaitForStatus = options.healthCheckWaitForStatus || 'yellow';
    this.healthCheckWaitForNodes = options.healthCheckWaitForNodes || '>=1';
    this.waitForActiveShards = options.waitForActiveShards || '1';
    this.pipeline = options.pipeline;
    this.retryLimit = options.retryLimit || 5;
    this.sendChunkSize = options.sendChunkSize || 200;

    this.bulk = []; // bulk to be flushed
    this.running = false;
    this.paused = false;
    this.timer = null;

    this._lastLog = null;
    debug('created', this);

    setTimeout(() => {
      this._emitBulkLength('timeout');
    }, 1000 * 60 * 10)
  }

  start() {
    this.running = true;
    this._emitBulkLength('start');
    this.checkEsConnection();
    debug('started');
  }

  stop() {
    this.running = false;
    this._emitBulkLength('stop');
    this._clearTimer();
    debug('stopped');
  }

  unpause() {
    this.paused = false;
    this._emitBulkLength('unpause');
    debug('unpaused');
  }

  pause() {
    this.paused = true;
    this._emitBulkLength('pause');
    this._clearTimer();
    debug('paused');
  }

  schedule() {
    if (!this._isBufferActive()) {
      return;
    }

    if (this.timer) {
      return;
    }

    this.timer = setTimeout(() => this.sendBuffer(), this.interval);
  }

  _clearTimer() {
    if (!this.timer) {
      return;
    }

    clearTimeout(this.timer);
    this.timer = null;
  }

  async sendBuffer() {
    this._clearTimer();
    this._emitBulkLength();

    if (this.bulk.length === 0) {
      debug('nothing to send');
      return;
    }

    if (!this._isBufferActive()) {
      return;
    }

    // write bulk to elasticsearch
    const chunk = this.bulk.splice(0, this.sendChunkSize);
    debug('bulk writer is going to write', chunk);
    try {
      await this.write(chunk);
      // write another chunk after successfully writing a chunk
      await this.sendBuffer();
    } catch(err) {
      // on error, try to set another schedule for retry
      this.schedule();
    }
  }

  _emitBulkLength(event) {
    if (!event && !_checkDateInterval(this.lastBulkLengthEmit, EMIT_BULK_LENGTH_INTERVAL)) {
      return;
    }

    this.lastBulkLengthEmit = new Date();
    /*this.transport.emit('bulkLength', {
      length: this.bulk.length,
      esConnection: this.esConnection
    });*/

    const lastLog = this._lastLog || _lastLog;
    if (lastLog) {
      this.logIndex = this.logIndex || 0;

      this.sendLogs([{
        index: lastLog.index,
        type: lastLog.type,
        attempts: 0,
        doc: {
          message: `Logs es bulkLength event (private), data: ${JSON.stringify({
            length: this.bulk.length,
            esConnection: this.esConnection,
            running: this.running,
            paused: this.paused,
            event,
            lastAppend: this._lastLog && this._lastLog.time || undefined
          })}`,
          severity: 'info',
          '@timestamp': new Date(),
          fields: {
            spServiceId: (lastLog.doc.fields || {}).spServiceId || -1,
            serviceName: (lastLog.doc.fields || {}).serviceName || 'unknown',
            pid: (lastLog.doc.fields || {}).pid || -1,
            logId: `${(lastLog.doc.fields || {}).pid || -1}aaaaaa${this.logIndex++}`,
            env: (lastLog.doc.fields || {}).env || 'unknown',
            AKS_POD_NAME: (lastLog.doc.fields || {}).AKS_POD_NAME || 'unknown',
            AKS_POD_IP: (lastLog.doc.fields || {}).AKS_POD_IP || 'unknown',
            AKS_NODE_NAME: (lastLog.doc.fields || {}).AKS_NODE_NAME || 'unknown'
          }
        }
      }]).catch(() => {});
    }
  }

  _isBufferActive() {
    this._emitBulkLength();

    if (this.bulk.length > 0 && this.esConnection) {
      return true;
    }

    this._retryCheckingConnection();
    return false;
  }

  _retryCheckingConnection() {
    if (this.esConnection || !this.running) {
      return;
    }

    if (_checkDateInterval(this.lastConnectionAttempt, CHECK_CONNECTION_INTERVAL)) {
      this.checkEsConnection();
    }
  }

  append(index, type, doc) {
    this._lastLog = _lastLog = { index, type, doc, time: new Date() };

    if (!this.running) {
      return;
    }

    if (this.paused) {
      this._retryCheckingConnection();
      return;
    }

    if (this.options.buffering === true) {
      if (
        typeof this.options.bufferLimit === 'number'
        && this.bulk.length >= this.options.bufferLimit
      ) {
        debug('message discarded because buffer limit exceeded');
        // @todo: i guess we can use callback to notify caller
        return;
      }

      this.bulk.push({
        index,
        type,
        doc,
        attempts: 0
      });

      // start scheduling once a log was appended
      this.schedule();
    } else {
      this.write([{ index, type, doc }]).catch(() => {/*do nothing*/});
    }
  }

  async write(chunk) {
    try {
      await this.sendLogs(chunk);
    } catch(err) {
      // rollback this.bulk array
      const itemsToRestore = [];
      for (const chunkItem of chunk) {
        if (chunkItem.attempts >= this.retryLimit) {
          debug('retry attempts exceeded');
          continue;
        }

        chunkItem.attempts++;
        itemsToRestore.push(chunkItem);
      }

      this.bulk.unshift(...itemsToRestore);
      if (this.options.bufferLimit && this.bulk.length >= this.options.bufferLimit) {
        this.bulk.splice(this.options.bufferLimit);
      }

      debug('error occurred during writing', err);
      this.pause();
      this.checkEsConnection();
      this.transport.emit('error', {
        esType: 'bulkError',
        error: err
      });
      try {
        this._emitBulkLength(`write error ${err.stack || JSON.stringify(err)}`);
      } catch(err) { }
      throw err;
    }
  }

  async sendLogs(chunk) {
    debug('writing to ES');
    const { body: res } = await this.client.bulk({
      body: chunk.reduce((body, { index, type, doc }) => {
        body.push(
          { index: { _index: index, _type: type, pipeline: this.pipeline } },
          doc
        );
        return body;
      }, []),
      waitForActiveShards: this.waitForActiveShards,
      timeout: this.interval + 'ms',
    });

    if (res && res.errors && res.items) {
      for (const item of res.items) {
        if (item.index && item.index.error) {
          debug('elasticsearch index error', item.index);
          throw new Error('ElasticSearch index error ' + JSON.stringify(item));
        }
      }
    }
  }

  checkEsConnection() {
    const thiz = this;
    thiz.esConnection = false;
    thiz.lastConnectionAttempt = new Date();

    const operation = retry.operation({
      forever: true,
      retries: 1,
      factor: 1,
      minTimeout: 1 * 1000,
      maxTimeout: 60 * 1000,
      randomize: false
    });
    return new Promise((fulfill, reject) => {
      operation.attempt((currentAttempt) => {
        thiz.lastConnectionAttempt = new Date();
        debug('checking for ES connection');
        thiz.client.cluster.health({
          timeout: thiz.healthCheckTimeout,
          wait_for_nodes: thiz.healthCheckWaitForNodes,
          wait_for_status: thiz.healthCheckWaitForStatus
        })
          .then(
            (res) => {
              thiz.esConnection = true;
              const start = () => {
                if (thiz.options.buffering === true) {
                  debug('starting bulk writer');
                  thiz.unpause();
                  thiz.sendBuffer();
                }
              };
              // Ensure mapping template is existing if desired
              if (thiz.options.ensureMappingTemplate) {
                thiz.ensureMappingTemplate((res1) => {
                  fulfill(res1);
                  start();
                }, reject);
              } else {
                fulfill(true);
                start();
              }
            },
            (err) => {
              debug('re-checking for connection to ES');
              if (operation.retry(err)) {
                return;
              }
              thiz.esConnection = false;
              debug('cannot connect to ES');
              this.transport.emit('error', {
                esType: 'checkFailure',
                error: err
              });
              reject(new Error('Cannot connect to ES'));
            }
          );
      });
    });
  }

  ensureMappingTemplate(fulfill, reject) {
    const thiz = this;

    const indexPrefix = typeof thiz.options.indexPrefix === 'function'
      ? thiz.options.indexPrefix()
      : thiz.options.indexPrefix;
    // eslint-disable-next-line prefer-destructuring
    let mappingTemplate = thiz.options.mappingTemplate;
    if (mappingTemplate === null || typeof mappingTemplate === 'undefined') {
      // es version 6 and below will use 'index-template-mapping-es-lte-6.json'
      // 7 and above will use 'index-template-mapping-es-gte-7.json'
      const esVersion = Number(thiz.options.elasticsearchVersion) >= 7 ? 'gte-7' : 'lte-6';
      const rawdata = fs.readFileSync(
        path.join(__dirname, 'index-template-mapping-es-' + esVersion + '.json')
      );
      mappingTemplate = JSON.parse(rawdata);
      mappingTemplate.index_patterns = indexPrefix + '-*';
    }

    const tmplCheckMessage = {
      name: 'template_' + indexPrefix
    };
    thiz.client.indices.existsTemplate(tmplCheckMessage).then(
      (res) => {
        if (res.statusCode && res.statusCode === 404) {
          const tmplMessage = {
            name: 'template_' + indexPrefix,
            create: true,
            body: mappingTemplate
          };
          thiz.client.indices.putTemplate(tmplMessage).then(
            (res1) => {
              fulfill(res1.body);
            },
            (err1) => {
              thiz.transport.emit('error', {
                esType: 'putTemplate',
                error: err1
              });
              reject(err1);
            }
          );
        } else {
          fulfill(res.body);
        }
      },
      (res) => {
        thiz.transport.emit('error', {
          esType: 'existsTemplate',
          error: res
        });
        reject(res);
      }
    );
  }
}

function _checkDateInterval(date, interval) {
  if (!date) {
    return true;
  }

  const intervalAgo = new Date((new Date()).getTime() - interval);
  return date < intervalAgo;
}

module.exports = BulkWriter;
